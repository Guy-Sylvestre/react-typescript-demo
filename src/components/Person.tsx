import { PersonProps } from "./Person.types";

const Person = (props: PersonProps) => {
    return (
        <>
            {props.name.first} {props.name.last}
        </>
    );
};

export default Person;