import { Name } from './Person.types'

type PersonListProps = {
    /**
     * Array abjet target
     */
    name: Name[]
}


const PersonList = (props: PersonListProps) => {
    return (
        <>
            {
                props.name.map((name) => {
                    return (
                        <h2 key={name.first}>{name.first + ` ` + name.last}</h2>
                    )
                })
            }
        </>
    );
};

export default PersonList;