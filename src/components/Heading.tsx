type HeadingProps = {
    /**
     * Children elements string type
     */
    children: string
}

const Heading = (props: HeadingProps) => {
    return (
        <>
            <h2>{props.children}</h2>
        </>
    );
};

export default Heading;