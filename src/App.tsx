import React from 'react';
import './App.css';
import Status from './components/Status';
import Heading from './components/Heading';
import Oscar from './components/Oscar';
import Greet from './components/Greet';
import Button from './components/Button';
import { log } from 'console';
import Input from './components/Input';
import Container from './components/Container';
// import Person from './components/Person';
// import PersonList from './components/PersonList';

function App() {

    const a:  string = 'vishwas'

    // const personName = {
    //     first: "bruce",
    //     last: "Wayne"
    // }
    // const nameList = [
    //     {
    //         first: "Bruce",
    //         last: "Wayne"
    //     },
    //     {
    //         first: "Clark",
    //         last: "Kent"
    //     },
    //     {
    //         first: "Princess",
    //         last: "Diana"
    //     }
    // ]
    

    return (
        <div className="App">
            <br /><br /><br /><br /><br />
            {/* <Person name={personName}/> */}
            {/* <PersonList name={nameList} /> */}
            <Status status="loading" />
            <Heading>Placeholder text</Heading>
            <Oscar>
                <Heading>Oscar goes Leonardo Dicpario!</Heading>
            </Oscar>
            <Greet name={a} isLogged={false}/>
            <Button handleClick={(event, id) => console.log('Hello world', event, id)} />
            <Input value='' handleChange={(event) => console.log(event)} />
            <Container style={{ border: '1px solid black', padding: '1rem' }} />
        </div>
    );
}

export default App;
