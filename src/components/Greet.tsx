type GreetProps = {
    /**
     * string, number, null and booleen target
     * messageCount is optional parameter
     */
    name: string | number | null,
    messageCount?: number,
    isLogged: boolean
}

function Greet(props: GreetProps) {

    const { messageCount = 0 } = props

    return (
        <>
            {
                props.isLogged ? <h2>Welcome {props?.name}! you have {messageCount} unread messages</h2> : 'Welcome Guest'
            }
        </>
    );
}

export default Greet;