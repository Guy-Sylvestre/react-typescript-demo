type OscarProps = {
    /**
     * Children elements for react reactNode
     */
    children: React.ReactNode
}

const Oscar = (props: OscarProps) => {
    return (
        <>
            <div>{props.children}</div>
        </>
    );
};

export default Oscar;