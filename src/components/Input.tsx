import React from "react";

type InputProps = {
    /**
     * Handle change events HTML inout element
     */
    value?: string,
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const Input = ({value, handleChange}: InputProps) => {

    return (
        <>
            <input type="text" value={value} onChange={handleChange} name="" id="" />
        </>
    );
};

export default Input;