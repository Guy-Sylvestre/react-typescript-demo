export type Name = {
    first: string,
    last: string,
}

export type PersonProps = {
    /**
     * object target
     */
    name: Name
}